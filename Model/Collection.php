<?php

namespace AzureSpring\Zowoyoo\Model;

use AzureSpring\Zowoyoo\Annotation\Template;

/** @Template({"T"}) */
class Collection
{
    /** @var array */
    private $elements = [];

    public function getElements(): array
    {
        return $this->elements;
    }
}
