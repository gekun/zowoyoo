<?php

namespace AzureSpring\Zowoyoo\Model;

use AzureSpring\Zowoyoo\Annotation\Template;

/** @Template({"T"}) */
class Unit implements Squeezable
{
    private $data;

    public function getData()
    {
        return $this->data;
    }

    public function squeeze()
    {
        return $this->data;
    }
}
