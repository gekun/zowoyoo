<?php

namespace AzureSpring\Zowoyoo;

use AzureSpring\Zowoyoo\Annotation\Template;
use AzureSpring\Zowoyoo\Model\Result;
use AzureSpring\Zowoyoo\Serializer\TemplateHandler;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Handler\HandlerRegistryInterface;
use JMS\Serializer\SerializerBuilder;

class Client
{
    private $guzzle;

    private $key;

    private $secret;

    private $serializer;

    private $contents;

    public function __construct(\GuzzleHttp\Client $guzzle, string $key, string $secret)
    {
        $this->guzzle = $guzzle;
        $this->key = $key;
        $this->secret = $secret;
        $this->serializer = SerializerBuilder::create()
            ->setMetadataDirs([
                'AzureSpring\\Zowoyoo\\Model' => __DIR__.'/Resources/config/serializer',
            ])
            ->configureHandlers(function (HandlerRegistryInterface $registry) {
                $registry->registerSubscribingHandler(new TemplateHandler());
            })
            ->addDefaultHandlers()
            ->build()
        ;
    }

    public function findProducts(Model\ProductFilter $filter, int $size = 50, int $page = 1, ?int $orderBy = null)
    {
        return $this->request(
            Template::bind(Model\Fragment::class, Model\Product::class),
            'GET',
            '/api/list.jsp',
            array_merge($filter->toParams(), [
                'pageNum' => $size,
                'pageNo' => $page,
                'orderBy' => $orderBy,
            ]),
            ['result.inline', 'products']
        );
    }

    public function findProduct(string $id)
    {
        return $this->request(
            Template::bind(Model\Unit::class, Model\Product::class),
            'GET',
            '/api/detail.jsp',
            ['productNo' => $id],
            ['product']
        );
    }

    public function getContents()
    {
        return $this->contents;
    }

    private function request(string $type, string $method, string $uri, array $params, array $serializationGroups = [])
    {
        $response = $this->guzzle->request($method, $uri, [
            'query' => array_merge($params, [
                'custId' => $this->key,
                'apikey' => $this->secret,
            ]),
        ]);

        $this->contents = $response->getBody()->getContents();
        $context = DeserializationContext
            ::create()
            ->setGroups(array_merge(['Default'], $serializationGroups))
        ;

        /** @var Result $result */
        $result = $this->serializer->deserialize($this->contents, Template::bind(Model\Result::class, $type), 'xml', $context);
        if (Result::STATUS_OK !== $result->getStatus()) {
            throw new Exception($result->getMessage());
        }

        return $result->getData()->squeeze();
    }
}
