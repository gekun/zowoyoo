<?php

namespace AzureSpring\Zowoyoo\Annotation;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
final class Template
{
    /**
     * @var array<string>
     * @Required
     */
    public $params;

    public static function bind(string $name, string ...$params)
    {
        return sprintf('template<%s<%s>>', $name, implode(', ', $params));
    }
}
